#!/usr/bin/env bash

set -e

echo -ne ' [                          ] (0%) \r'

pip3 install sagan --upgrade &>/dev/null

echo -ne ' [#####                     ] (20%)\r'

install_path="/opt/sagan-control-daemon/"
# Install / update control daemon
if [ ! -d ${install_path} ]; then
    mkdir ${install_path}
fi

if [ ! -d ${install_path}/.git ]; then
   git clone https://bitbucket.org/quberider/sagan-control-daemon.git ${install_path} &>/dev/null
fi

echo -ne ' [###########               ] (40%)\r'


cd ${install_path}
git pull origin master &>/dev/null
sh ./install.sh &>/dev/null

echo -ne ' [####################      ] (80%)\r'

./start.sh  &>/dev/null

echo -ne ' [#######################   ] (90%)\r'

cd /home/pi/Desktop
echo "
[*]
wallpaper_mode=fit
wallpaper_common=1
wallpaper=/home/pi/Documents/cuberider/sagandeployment/space.jpg
desktop_bg=#000000
desktop_fg=#e8e8e8
desktop_font=Roboto Light 12
show_wm_menu=0
sort=mtime;ascending;
show_trash=1
show_mounts=0

[raspbian.zip]
x=3
y=148
" > /home/pi/.config/pcmanfm/LXDE-pi/desktop-items-0.conf

touch /home/pi/Desktop/update.desktop
chmod +x /home/pi/Desktop/update.desktop

chown pi ./update.desktop

echo "[Desktop Entry]
Exec=sudo /home/pi/Documents/cuberider/sagandeployment/task_runner.sh
Icon=/home/pi/Documents/cuberider/sagandeployment/update_icon.png
Name=Cuberider Update
Type=Application
Terminal=true
" > /home/pi/Desktop/update.desktop

echo "[Desktop Entry]
Exec=sudo bash -c \"/opt/sagan-control-daemon/start.sh 1>/dev/null;\
echo \\\"Starting remote experiments. The light on you Sagan should start flashing.\\\"; sleep 10;\"
Icon=/home/pi/Documents/cuberider/sagandeployment/update_icon.png
Name=Start Remote Experiments
Type=Application
Terminal=true
" > /home/pi/Desktop/start.desktop

echo "[Desktop Entry]
Exec=sudo bash -c \"/opt/sagan-control-daemon/stop.sh 1>/dev/null; sleep 10;\"
Icon=/home/pi/Documents/cuberider/sagandeployment/update_icon.png
Name=Stop Remote Experiments
Type=Application
Terminal=true
" > /home/pi/Desktop/stop.desktop

echo -ne ' [#########################] (100%)\n'
