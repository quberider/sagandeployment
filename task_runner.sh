#!/bin/bash
# THIS FILE IS USED TO RUN CUBERIDER UPDATES AND SYSTEM MODIFICATIONS, PLEASE DO NOT MODIFY THIS FILE IN ANY WAY :)
# update the sagan libraries to the latest version

echo "Updating your Sagan..."

cd /home/pi/Documents/cuberider
sudo git clone https://bitbucket.org/quberider/sagandeployment.git &>/dev/null

cd /home/pi/Documents/cuberider/sagandeployment/
sudo git pull &>/dev/null


sudo apt-get update 1>/dev/null
./update-tasks.sh

if [ $? -eq 0 ]; then
    echo -ne 'SYSTEM REBOOT in 5 seconds        \r'
fi

sleep 5
sudo reboot

